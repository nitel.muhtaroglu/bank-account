package cs102;

public class Account {
    private int number;
    private double balance;
    private String currency;
    private boolean active;

    // Constructors
    public Account(int n, double b, String c) {
        number = n;
        if (b > 0) {
            balance = b;
        } else {
            balance = 0;
        }

        checkSetCurrency(c);
        active = true;
    }

    public Account(int n, String c) {
        number = n;
        balance = 0;

        checkSetCurrency(c);
        active = true;
    }

    public Account(int n) {
        number = n;
        balance = 0;
        currency = "TL";
        active = true;
    }

    public void deposit(double d) {
        if (d > 0) {
            balance = balance + d;
            System.out.println(d + " " + currency + " have been deposited");
            System.out.println("The balance is " + balance + " " + currency);
        } else {
            System.out.println("The amount should be positive!");
        }
    }

    public void deposit() {
        balance = balance + 0;
    }

    public void withdraw(double w) {
        if (w > 0) {
            if (balance < w) {
                System.out.println("Account does not have " + w + " " + currency);
            } else {
                balance = balance - w;
                System.out.println(w + " " + currency + " have been withdrawn");
                System.out.println("The balance is " + balance + " " + currency);
            }
        } else {
            System.out.println("The amount should be positive!");
        }
    }

    public void report() {
        System.out.println("Account " + number +
                " has " + balance + " " +
                currency + ".");
    }

    public int getNumber() {
        return number;
    }

    public double getBalance() {
        return balance;
    }

    public String getCurrency() {
        return currency;
    }

    public boolean isActive() {
        return active;
    }

    public void setCurrency(String c) {
        if (currency.equals("TL") && c.equals("USD")) {
            balance = balance / 2.9;
        }

        if (currency.equals("USD") && c.equals("TL")) {
            balance = balance * 2.9;
        }

        if (c.equals("TL") || c.equals("USD")) {
            currency = c;
        }
    }

    public void setActive(boolean a) {
        active = a;
    }

    private void checkSetCurrency(String c) {
        if (c.equals("USD")) {
            currency = c;
        } else {
            currency = "TL";
        }
    }

    public String toString() {
        return "Account " + number + ": " + balance + " " + currency;
    }
}
